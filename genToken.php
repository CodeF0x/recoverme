<?php
/**
 * This file is used to generate another token for users who lost theirs.
 * Deploy this file on a webserver with PHP installed and access it via your browser.
 */
$token;
$hashedToken;

function generateToken() {
    $GLOBALS['token']= bin2hex(random_bytes(60));
    return $GLOBALS['token'];
}

function getHash() {
    $GLOBALS['hashedToken'] = password_hash($GLOBALS['token'], PASSWORD_BCRYPT);
    return $GLOBALS['hashedToken'];
}

echo "Token for the user: " . generateToken() . "<br><br>Hashed token to put in the database: " . getHash(); 
if (password_verify($token, $hashedToken)) {
    echo "<br><br>Tokens are equal and save to use.";
} else {
    echo "<br><br>Tokens are not save to use. Please generate another one.";
}