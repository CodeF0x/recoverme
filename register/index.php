<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Include external files
include 'php/generateHTML.php';
include 'php/sql.php';
include 'php/sendMail.php';

session_start();

$config = array(
    "domain" => "vimail.cc"
);
$ret;
//Check for POST
generateHead();
if (isset($_SESSION['email']) && isset($_POST['username'])) {
    //body();
    $_SESSION['message'] = 'You already registered an account. Please try again later.';
}

// Captcha
$captcha = 'not solved yet';

if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['g-recaptcha-response'])) {

    $captcha = $_POST['g-recaptcha-response'];
    //Process a registration.

    $secretKey = "6LeebnkUAAAAAE4Z55NtYcQfnjigep588Ijb8g6I";
    $ip = $_SERVER['REMOTE_ADDR'];
    $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $secretKey . "&response=" . $captcha . "&remoteip=" . $ip);
    $responseKeys = json_decode($response, true);
    if (intval($responseKeys["success"]) !== 1) {
        $_SESSION['message'] = 'Invalid captcha. Please try again.';
    } else {
        if (!isset($_SESSION['email'])) {
            $GLOBALS['ret'] = register($_POST['username'], $_POST['password']);
            if ($GLOBALS['ret'] === 'already taken') {
                $_SESSION['message'] = 'This username is already taken. Please choose a different one.';
            } else if ($GLOBALS['ret'] === 'reserved name') {
                $_SESSION['message'] = 'This is a reserved word you cannot have as a username. Sorry!';
            } else if ($GLOBALS['ret'] === 'something went wrong') {
                $_SESSION['message'] = 'Someting went wrong, please try again later.';
            } else if ($GLOBALS['ret'] === true) {
                connectToRecoverme();
                $_SESSION['message'] = 'E-mail ' . $_POST['username'] . '@' . $config['domain'] . ' successfully created!';
                $_SESSION['email'] = $_POST['username'] . "@" . $config['domain'];
                saveToDatabase($_SESSION['email'], $_POST['recovery-email']);
                sendRecoveryMail();
                session_destroy();
            } else {
                $_SESSION['message'] = 'There was an error creating the email, please try again or contact the support to assist you! Error message: ' .  $GLOBALS['ret'];
            }
        }
    }
}
generateBody();

function register($username, $password)
{

    if (needToAbort($username, $password)) {
        die("Validation unsuccessful.");
    }

    global $config;
    $username = clean($username);

    // Check if username is reserved
    $handle = fopen("./other/reserved-names.txt", "r");
    if ($handle) {
        while (($line = fgets($handle)) !== false) {
            if (trim($line) == $username) {
                $bool = 'reserved name';
                return $bool;
            }
        }
        fclose($handle);
    } else {
        $bool = 'something went wrong';
        return $bool;
    }
    $password = escapeshellcmd($password);
    $return = shell_exec("curl -X POST -d 'email=$username@vimail.cc' -d 'password=$password' https://vimail.cc/admin/mail/users/add -k --user service@vimail.cc:VimailCC2018!");
    $bool = false;

    if (strpos($return, 'mail user added') !== false) {
        $bool = true;
    } else if (strpos($return, 'User already exists.') !== false) {
        $bool = 'already taken';
    } else {
        $bool = false;
    }
    return $bool;
}

function clean($string)
{
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
    $string = trim($string);
    $string = preg_replace('/[^A-Za-z0-9.\-]/', '', $string); // Removes special chars except dots.

    return $string;
}

function needToAbort($username, $password) {
    $regex = '/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/';

    if (preg_match($regex, $username)) {
        return true;
    } else if (strlen($password) < 8) {
        return true;
    } else if (!isset($_POST['agree-term'])) {
        return true;
    }
    return false;
}