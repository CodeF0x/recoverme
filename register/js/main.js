// Error banner for form validation
const error = document.getElementById('error_banner');

document.getElementById('email').addEventListener('keyup', function() {
  this.value = this.value.toLowerCase();
});

document.getElementById('recovery-email').addEventListener('keyup', function() {
  this.value = this.value.toLowerCase();
});

/** Validation */
function validate(event) {
  error.firstElementChild.innerText = '';

  let email = document.getElementById('email').value;
  let password = document.getElementById('pass').value;
  let passwordRepeated = document.getElementById('re_pass').value;
  let acceptedTerms = document.getElementById('agree-term').checked;
  let recoveryMail = document.getElementById('recovery-email').value;

  const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if (email !== '' && password !== '' && passwordRepeated !== '') {
    if (regex.test(String(email).toLowerCase())) {
      event.preventDefault();
      showError(
        'Enter only the username you want to have. (Without @vimail.cc)'
      );
    } else if (/[^A-Za-z0-9.\-]/.test(String(email).toLowerCase())) {
      event.preventDefault();
      showError(
        'Only letters from A to Z, numbers from 0 to 9, and dots are allowed.'
      );
    } else if (password !== passwordRepeated) {
      event.preventDefault();
      showError("Your passwords don't match each other, enter them again.");
    } else if (password.length < 8) {
      event.preventDefault();
      showError('Your password must have a minimum length of 8.');
    } else if (recoveryMail !== '') {
      if (!regex.test(String(recoveryMail).toLowerCase())) {
        event.preventDefault();
        showError('Your recovery mail is not a valid email address.');
      }
    } else if (!acceptedTerms) {
      event.preventDefault();
      showError('You must agree to our terms of service.');
    }
  } else {
    event.preventDefault();
    showError('You must fill out every field.');
  }
}

function showError(text) {
  error.firstElementChild.innerText = text;
  error.style.display = 'block';
}
