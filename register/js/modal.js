// Variables for modal to notify user after submitting form
const modal = document.getElementById('modal');
const modalText = document.getElementById('modal-text');
const closeButton = document.getElementById('close');

const infoLink = document.getElementById('info');
infoLink.addEventListener('click', function(e) {
    e.preventDefault();
    notifyUser('When registering an account, you will receive a token that is required to change or reset your password. If one is entered, this token will be sent your recovery email too.');
});

closeButton.onclick = closeModal;

/** Modal */
function notifyUser(text) {
    modal.style.display = 'block';
    modalText.innerText = text;

    if (text.includes('successfully created!')) {
        let link = document.createElement('a');
        link.href = 'https://vimail.cc/mail';
        link.innerText = ' Click here to login!';

        modalText.appendChild(link);
    }
}

function closeModal() {
    modal.style.display = 'none';
}

// When user clicks outside of modal, close the modal
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = 'none';
    }
};
