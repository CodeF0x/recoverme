<?php
function template() {
    $token = $_SESSION['token'];
    return <<<EOD
    <!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width" />
    <!--[if !mso]>
        <!-->
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,300&subset=latin"
      rel="stylesheet"
      type="text/css"
    />
    <!--<![endif]-->
    <title>ViMail - Password Reset</title>
    <style type="text/css">
      * {
        margin: 0;
        padding: 0;
        font-family: 'OpenSans-Light', 'Helvetica Neue', 'Helvetica', Calibri,
          Arial, sans-serif;
        font-size: 100%;
        line-height: 1.6;
      }

      img {
        max-width: 100%;
      }

      body {
        -webkit-font-smoothing: antialiased;
        -webkit-text-size-adjust: none;
        width: 100% !important;
        height: 100%;
        word-break: normal;
      }

      a {
        color: #348eda;
      }

      .btn-primary {
        text-decoration: none;
        color: #fff;
        background-color: #a55bff;
        border: solid #a55bff;
        border-width: 10px 20px;
        line-height: 2;
        font-weight: bold;
        margin-right: 10px;
        text-align: center;
        cursor: pointer;
        display: inline-block;
      }

      .last {
        margin-bottom: 0;
      }

      .first {
        margin-top: 0;
      }

      .padding {
        padding: 10px 0;
      }

      table.body-wrap {
        width: 100%;
        padding: 0px;
        padding-top: 20px;
        margin: 0px;
      }

      table.body-wrap .container {
        border: 1px solid #f0f0f0;
      }

      table.footer-wrap {
        width: 100%;
        clear: both !important;
      }

      .footer-wrap .container p {
        font-size: 12px;
        color: #666;
      }

      table.footer-wrap a {
        color: #999;
      }

      .footer-content {
        margin: 0px;
        padding: 0px;
      }

      h1,
      h2,
      h3 {
        color: #660099;
        font-family: 'OpenSans-Light', 'Helvetica Neue', Helvetica, Arial,
          'Lucida Grande', sans-serif;
        line-height: 1.2;
        margin-bottom: 15px;
        margin: 40px 0 10px;
        font-weight: 200;
      }

      h1 {
        font-family: 'Open Sans Light';
        font-size: 45px;
      }

      h2 {
        font-size: 28px;
      }

      h3 {
        font-size: 22px;
      }

      p,
      ul,
      ol {
        margin-bottom: 10px;
        font-weight: normal;
        font-size: 14px;
      }

      ul li,
      ol li {
        margin-left: 5px;
        list-style-position: inside;
      }

      .container {
        display: block !important;
        max-width: 600px !important;
        margin: 0 auto !important;
        clear: both !important;
      }

      .body-wrap .container {
        padding: 0px;
      }

      .content,
      .footer-wrapper {
        max-width: 600px;
        margin: 0 auto;
        padding: 20px 33px 20px 37px;
        display: block;
      }

      .content table {
        width: 100%;
      }

      .content-message p {
        margin: 20px 0px 20px 0px;
        padding: 0px;
        font-size: 22px;
        line-height: 38px;
        font-family: 'OpenSans-Light', Calibri, Arial, sans-serif;
      }

      .preheader {
        display: none !important;
        visibility: hidden;
        opacity: 0;
        color: transparent;
        height: 0;
        width: 0;
      }

      code {
        color: #33cccc;
        font-weight: bold;
        text-decoration: none;
        font-size: 16px;
      }

      .code-wrap {
        word-break: break-all;
      }
    </style>
  </head>

  <body bgcolor="#f6f6f6">
    <span
      class="preheader"
      style="display: none !important; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;"
    >
      Never loose access again!
    </span>
    <!-- body -->
    <table class="body-wrap" width="600">
      <tr>
        <td class="container" bgcolor="#FFFFFF">
          <!-- content -->
          <table
            border="0"
            cellpadding="0"
            cellspacing="0"
            class="contentwrapper"
            width="600"
          >
            <tr>
              <td style="height:25px;">
                <img
                  border="0"
                  src="https://gallery.mailchimp.com/d42c37cf5f5c0fac90b525c8e/images/96288204-f67c-4ba2-9981-1be77c9fa18b.png"
                  width="600"
                />
              </td>
            </tr>
            <tr>
              <td>
                <div class="content">
                  <table class="content-message">
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td align="left">
                        <a href="https://vimate.io">
                          <img
                            src="https://vimate.io/wp-content/uploads/2019/01/vimate-logo.png"
                            width="126"
                            border="0"
                          />
                        </a>
                      </td>
                    </tr>
                    <tr>
                      <td
                        class="content-message"
                        style="font-family:'Open Sans Light',Calibri, Arial, sans-serif; color: #595959;"
                      >
                        <p>&nbsp;</p>
                        <p>
                          <img
                            width="190"
                            height="65"
                            src="https://gallery.mailchimp.com/d42c37cf5f5c0fac90b525c8e/images/70ea32b8-20b3-4eb9-88d5-72089d8129d3.png"
                            alt="Your VIMATE reset token"
                            border="0"
                          />
                        </p>
                        <h1
                          style="font-family:'OpenSans-Light', Helvetica, Calibri, Arial, sans-serif;"
                        >
                          Your ViMail password reset token
                        </h1>
                        <p
                          style="font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif; font-size:18px; line-height:26px;"
                        >
                          Thank you for registering a ViMail Account!<br />If
                          you should ever forget your password, please use the
                          token<br />
                        </p>
                        <div class="code-wrap">
                          <code style="word-break: break-all;">$token</code>
                        </div>
                        <br />
                        <span style="font-size: 18px;"
                          >to reset your password. Please keep this token
                          somewhere safe, where you can always access it,
                          because without your token, you can't reset your
                          password.</span
                        >
                        <p
                          style="font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif; font-size:18px; line-height:26px;"
                        >
                          &nbsp;
                        </p>
                        <table
                          width="325"
                          border="0"
                          cellspacing="0"
                          cellpadding="0"
                        >
                          <tr>
                            <td
                              width="325"
                              height="60"
                              bgcolor="#31cccc"
                              style="text-align:center;"
                            >
                              <a
                                href="https://vimail.cc/mail/recoverme"
                                align="center"
                                style="display:block; font-family:'Open Sans',Calibri, Arial, sans-serif;; font-size:20px; color:#ffffff; text-align: center; line-height:60px; display:block; text-decoration:none;"
                                >Reset my password now</a
                              >
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                          </tr>
                        </table>

                        <p
                          style="font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif; font-size:18px; line-height:26px;"
                        >
                          <img
                            border="0"
                            src="https://gallery.mailchimp.com/d42c37cf5f5c0fac90b525c8e/images/4d4431c8-e778-47ac-a026-a869106b2903.gif"
                            height="50"
                            width="200"
                          />
                        </p>
                        <p
                          style="font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif; font-size:18px; line-height:26px;"
                        >
                          If you need any further assistance with resetting your
                          password or with the ViMail RecoverMe! Service, please
                          email us at
                          <a
                            href="mailto:support@vimate.io"
                            style="color:#33CCCC; font-weight:bold; text-decoration:none; "
                            >support@vimate.io</a
                          >.
                        </p>
                        <p
                          style="font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif; font-size:18px; line-height:26px;"
                        >
                          <img
                            border="0"
                            src="https://gallery.mailchimp.com/d42c37cf5f5c0fac90b525c8e/images/4d4431c8-e778-47ac-a026-a869106b2903.gif"
                            height="50"
                            width="200"
                          />
                        </p>
                      </td>
                    </tr>
                  </table>
                </div>
              </td>
            </tr>
            <tr>
              <td
                bgcolor="#F7F7F7"
                style="max-width:600px; margin:0 auto; padding:20px 33px 20px 37px; display:block;"
              >
                <table cellspacing="0" cellpadding="10" width="100%">
                  <tr>
                    <td
                      colspan="3"
                      height="18"
                      style="font-size:1px; line-height:1px;"
                    >
                      &nbsp;
                    </td>
                  </tr>
                  <tr>
                    <td align="left" colspan="3">
                      <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                          <td>
                            <table>
                              <tr>
                                <td>
                                  <div style="float:left; margin:5px;">
                                    <a
                                      href="https://twitter.com/vimateio"
                                      title="VIMATE on Twitter"
                                      target="new"
                                    >
                                      <img
                                        src="https://gallery.mailchimp.com/d42c37cf5f5c0fac90b525c8e/images/e78ba6bd-5b0f-4d69-8fcf-acc064cbf7ea.png"
                                        width="29"
                                        height="29"
                                        border="0"
                                        alt="VIMATE on Twitter"
                                      />
                                    </a>
                                  </div>
                                </td>
                                <td>
                                  <div style="float:left; margin:5px;">
                                    <a
                                      href="https://www.instagram.com/vimateio/"
                                      title="VIMATE on Instagram"
                                      target="new"
                                    >
                                      <img
                                        src="https://gallery.mailchimp.com/d42c37cf5f5c0fac90b525c8e/images/9f72bdd3-5361-4f76-b683-6633a1c29145.png"
                                        width="29"
                                        height="29"
                                        border="0"
                                        alt="VIMATE on Instagram"
                                      />
                                    </a>
                                  </div>
                                </td>
                              </tr>
                            </table>
                          </td>
                          <td align="right" style="text-align:right;">
                            <a
                              href="mailto:support@vimate.io"
                              style="text-decoration: none; font-size:10px; font-weight:bold; font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif;color: #414141;"
                              title="support@vimate.io"
                              >support@vimate.io</a
                            >
                            <span
                              style="text-decoration: none; font-size:9px; font-weight:bold; font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif;color: #414141;"
                              >|</span
                            >
                            <a
                              style="text-decoration: none; font-size:10px; font-weight:bold; font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif;color: #414141;"
                              href="tel:+4915905300456"
                              >+49 1590 5300456</a
                            >
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td
                      colspan="3"
                      height="18"
                      style="font-size:1px; line-height:1px;"
                    >
                      &nbsp;
                    </td>
                  </tr>
                  <tr>
                    <td
                      style="font-size:10px; font-weight:normal; font-family: 'OpenSans', helvetica, sans-serif; color: #414141; text-align:left;"
                      align="left"
                    >
                      <a
                        href="https://vimate.io/privacy-policy"
                        style="text-decoration:none;font-size:10px; font-weight:normal; font-family: 'OpenSans', helvetica, sans-serif; color: #414141;"
                        >Privacy Policy</a
                      >
                    </td>
                    <td
                      colspan="2"
                      style="font-size:10px; font-weight:normal; font-family: 'OpenSans', helvetica, sans-serif; color: #414141; text-align:right;"
                      align="right"
                    >
                      Austrasse 3, 83071 Stephanskirchen &copy; 2019 VIMATE
                      Group
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="height:25px;">
                <img
                  width="600"
                  src="https://gallery.mailchimp.com/d42c37cf5f5c0fac90b525c8e/images/4c1b3727-e048-4e80-815b-a9197acc62fe.png"
                />
              </td>
            </tr>
            <tr>
              <td>
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td colspan="3">&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="25"></td>
                    <td>
                      <span
                        style="font-size:8px; font-weight:normal; font-family: 'OpenSans', helvetica, sans-serif; color: #535352;"
                      >
                        You received this E-Mail because you registered an
                        account at ViMail. ViMail is a service by VIMATE Group.
                        All rights reserved.
                      </span>
                    </td>
                    <td width="25"></td>
                  </tr>
                  <tr>
                    <td colspan="3">&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="3">&nbsp;</td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>

          <!-- /content -->
        </td>
        <td></td>
      </tr>
    </table>

    <!-- /body -->
  </body>
</html>
EOD;
}

function sendRecoveryMail() {
    $receiver = $_POST['username'];
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'To: '. $receiver .' <' . $receiver .'>' . "\r\n";
    $headers .= 'From: no-reply@vimail.cc <no-reply@vimail.cc>' . "\r\n";
    $subject = "Your recovery token from VIMAIL";
    $text = template();
    
    mail($receiver, $subject, $text, $headers);

    if (isset($_POST['recovery-email'])) {
        $receiver = $_POST['recovery-email'];
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'To: '. $receiver . ' <' . $receiver .'>' . "\r\n";
        $headers .= 'From: no-reply@vimail.cc <no-reply@vimail.cc>' . "\r\n";
        $subject = "Your recovery token from VIMAIL";
        $text = template();
        
        mail($receiver, $subject, $text, $headers);
    }
}