<?php
/**
 * This is used to generate the content of the HTML registration page.
 */
function generateHead()
{
    echo '';
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>ViMail | Register</title>

        <!-- Font Icon -->
        <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">

        <!-- Main css -->
        <link rel="stylesheet" href="css/style.css">

        <!-- Captcha -->
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    </head>

    <?php
}

function generateBody()
{
    echo '';
    ?>
    <body>

    <!-- Modal to notify user -->
    <div id="modal">
        <div id="modal-content">
            <span id="close">&times</span>
            <p id="modal-text"></p>
        </div>
    </div>

    <div class="main">

        <!-- Sign up form -->
        <section class="signup">
            <div class="container">
                <div class="signup-content">
                    <div class="signup-form">
                        <noscript>
                            <p>In order to register yourself an account, you must enable JavaScript in your browser.</p>
                        </noscript>
                        <h2 class="form-title">Sign up</h2>
                        <form method="POST" action="" class="register-form" id="register-form"
                              onsubmit="validate(event)">
                            <div class="form-group">
                                <label for="email"><i class="zmdi zmdi-email"></i></label>
                                <input type="text" name="username" id="email" placeholder="Your username"/>
                            </div>
                            <div class="form-group">
                                <label for="pass"><i class="zmdi zmdi-lock"></i></label>
                                <input type="password" name="password" id="pass" placeholder="Password"/>
                            </div>
                            <div class="form-group">
                                <label for="re-pass"><i class="zmdi zmdi-lock-outline"></i></label>
                                <input type="password" name="re_pass" id="re_pass" placeholder="Repeat your password"/>
                            </div>
                            <div class="form-group">
                                <label for="recover-email"><i class="zmdi zmdi-email"></i></label>
                                <input type="text" name="recovery-email" id="recovery-email" placeholder="Recovery email (optional)"/>
                            </div>
                            <div class="form-group">
                                <a href="#" id="info">Why whould I need a recovery email?</a>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="agree-term" id="agree-term" class="agree-term"/>
                                <label for="agree-term" class="label-agree-term"><span><span></span></span>I agree all
                                    statements in <a href="https://vimate.io/tos" target="_blank"
                                                     class="term-service">Terms of service</a></label>
                            </div>
                            <div class="form-group form-button">
                                <input type="submit" name="signup" id="signup" class="form-submit" value="Register"/>
                            </div>
                            <div class="g-recaptcha" data-sitekey="6LeebnkUAAAAAPXtMCPnz8iVx-EKLnoXboM1IJgh"></div>
                            <script src="js/modal.js"></script>
                            <div id="error_banner">
                                <span>
                                    <?php
                                    if (isset($_SESSION['message']) && $_SESSION['message'] !== '') {
                                        ?>
                                        <script>
                                            notifyUser('<?php echo $_SESSION['message']; ?>');
                                        </script>
                                        <?php
                                        $_SESSION['message'] = '';
                                    }
                                    ?>
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="signup-image">
                        <figure><img src="images/signup-image.jpg" alt="sing up image"></figure>
                        <a href="https://vimail.cc/mail" class="signup-image-link">Already registered?</a>
                    </div>
                </div>
            </div>
        </section>

    </div>
    <script src="js/main.js"></script>
    </body>
    <!-- This templates was made by Colorlib (https://colorlib.com) and got extended by VIMATE (https://vimate.io) -->
    </html>
    <?php
}
