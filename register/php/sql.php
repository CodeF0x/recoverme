<?php
$GLOBALS['connection'] = null;
function connectToRecoverme() {
    $GLOBALS['connection'] = new mysqli("localhost", "recoverme", "zuckerzucker", "recoverme");
    if ($GLOBALS['connection']->connect_errno) {
        die("Verbindung fehlgeschlagen: " . $GLOBALS['connection']->connect_error);
    }
}


function generateToken() {
    $_SESSION['token'] = bin2hex(random_bytes(60));
}

function getHash() {
    return password_hash($_SESSION['token'], PASSWORD_BCRYPT);
}

function saveToDatabase($email, $recoveryMail = 'NULL') {
    generateToken();
    $hash = getHash();

    $sql = "INSERT INTO dataset (email, token, recovery_email) VALUES (?, ?, ?)";
    $statement = $GLOBALS['connection']->prepare($sql);
    $statement->bind_param('sss', $email, $hash, $recoveryMail);
    
    $statement->execute();
    $GLOBALS['connection']->close();
}