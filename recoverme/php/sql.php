<?php
$GLOBALS['connection'] = null;
function connectToRecoverme() {
    $GLOBALS['connection'] = new mysqli("localhost", "recoverme", "zuckerzucker", "recoverme");
    if ($GLOBALS['connection']->connect_errno) {
        die("Verbindung fehlgeschlagen: " . $GLOBALS['connection']->connect_error);
    }
}

function emailExists() {
    $email = $_POST['email'] . "@vimail.cc";
    $sql = "SELECT * FROM dataset WHERE email = ?";
    $statement = $GLOBALS['connection']->prepare($sql);

    $statement->bind_param('s', $email);
    $statement->execute();
    $result = $statement->get_result();
        
    while($row = $result->fetch_object()) {
       if ($row->email) {
           return true;
       }
    }
    return false;
}

function tokenIsCorrect() {
    $email = $_POST['email'] . "@vimail.cc";
    $sql = "SELECT * FROM dataset WHERE email = ?";
    $statement = $GLOBALS['connection']->prepare($sql);

    $statement->bind_param('s', $email);
    $statement->execute();
    $result = $statement->get_result();
        
    while($row = $result->fetch_object()) {
       if (password_verify($_POST['token'], $row->token)) {
           return true;
       }
    }
    return false;
}

function closeConnection() {
    $GLOBALS['connection']->close();
}