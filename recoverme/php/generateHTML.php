<?php 
function generateHead() {
    ?>
    <!DOCTYPE html>
    <html lang="en">

        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
                crossorigin="anonymous">
            <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">
            <link rel="stylesheet" href="./css/main.css">
            <title>ViMail | Reset password</title>
        </head>
    <?php
}

function generateBody() {
    ?>
    <!-- Modal to notify user -->
    <div id="modal">
        <div id="modal-content">
            <span id="close">&times</span>
            <p id="modal-text"></p>
        </div>
    </div>

    <div class="main">

        <!-- Sign up form -->
        <section class="signup">
            <div class="container">
                <div class="signup-content">
                    <div class="signup-form">
                        <noscript>
                            <p>In order to reset your password, you must enable JavaScript in your browser.</p>
                        </noscript>
                        <h2 class="form-title">Reset password</h2>
                        <form method="post" action="" class="register-form" id="register-form" onsubmit="validate(event)">
                            <div class="form-group">
                                <label for="email"><i class="zmdi zmdi-email"></i></label>
                                <input type="text" name="email" id="email" placeholder="Your username"/>
                            </div>
                            <div class="form-group">
                                <label for="token"><i class="zmdi zmdi-key"></i></label>
                                <input type="text" name="token" id="token" placeholder="Your token"/>
                            </div>
                            <div class="form-group">
                                <label for="pass"><i class="zmdi zmdi-lock"></i></label>
                                <input type="password" name="password" id="password" placeholder="New password"/>
                            </div>
                            <div class="form-group">
                                <label for="re-pass"><i class="zmdi zmdi-lock-outline"></i></label>
                                <input type="password" name="password-repeat" id="password-repeat" placeholder="Repeat your new password"/>
                            </div>
                            <div class="form-group form-button">
                                <input type="submit" name="signup" id="signup" class="form-submit" value="Reset"/>
                            </div>
                            <script src="js/modal.js"></script>
                            <div id="error_banner">
                            <span>
                                <?php
                                    if (isset($_SESSION['message']) && $_SESSION['message'] !== '') {
                                        ?>
                                        <script>
                                            notifyUser('<?php echo $_SESSION['message']; ?>');
                                        </script>
                                        <?php
                                        $_SESSION['message'] = '';
                                    }
                                ?>
                            </span>
                            </div>
                        </form>
                    </div>
                    <div class="signup-image">
                        <figure><img src="images/marginalia-information-security.png" alt="sing up image"></figure>
                    </div>
                </div>
            </div>
        </section>

    </div>
    <script src="js/main.js"></script>
    </body>
    <!-- This templates was made by Colorlib (https://colorlib.com) and got extended by VIMATE (https://vimate.io) -->
    </html>
    <?php
}
