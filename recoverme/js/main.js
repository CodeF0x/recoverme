// Error banner for form validation
const error = document.getElementById('error_banner');

document.getElementById('email').addEventListener('keyup', function() {
    this.value = this.value.toLowerCase();
});


/** Validation */
function validate(event) {
  error.firstElementChild.innerText = '';

  let email = document.getElementById('email').value;
  let password = document.getElementById('password').value;
  let passwordRepeated = document.getElementById('password-repeat').value;
  let token = document.getElementById('token').value;

  const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if (
    email !== '' &&
    password !== '' &&
    passwordRepeated !== '' &&
    token !== ''
  ) {
    if (regex.test(String(email).toLowerCase())) {
      event.preventDefault();
      showError('Enter only the username. (Without @vimail.cc)');
    } else if (password !== passwordRepeated) {
      event.preventDefault();
      showError("Your passwords don't match each other, enter them again.");
    } else if (password.length < 8) {
      event.preventDefault();
      showError('Your password must have a minimum length of 8 characters.');
    } else if (token === '') {
      event.preventDefault();
      showError('Enter your reset token.');
    } else if (token.length < 120) {
      event.preventDefault();
      showError('Your token is too short. (Must be 120 characters)');
    } else if (token.length > 120) {
      event.preventDefault();
      showError('Your token is too long. (Must be 120 characters)');
    }
  } else {
    event.preventDefault();
    showError('You must fill out every field.');
  }
}

function showError(text) {
  error.firstElementChild.innerText = text;
  error.style.display = 'block';
}
