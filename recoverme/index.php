<?php
/** INITIALISATION */

include 'php/generateHTML.php';
include 'php/sql.php';

generateHead();

/** IMPLEMENTATION */

function needToAbort($username, $password) {
    $regex = '/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/';

    if (preg_match($regex, $username)) {
        return true;
    } else if (strlen($password) < 8) {
        return true;
    }
    return false;
}


/** LOGIC */

if (isset($_POST['email']) && isset($_POST['token']) && isset($_POST['password']) && $_POST['password-repeat']) {
    if (needToAbort($_POST['email'], $_POST['password'])) {
        die("Validation unsuccessful.");
    }

    session_start();

    connectToRecoverme();
    // Check if email exists in database
    if (emailExists()) {
        if (tokenIsCorrect()) {
            // Change password
            $username = $_POST['email'];
            $domain = "vimail.cc";
            $password = $_POST['password'];
            $returnValue = shell_exec('curl -X POST -d "email=' . $username . '@' . $domain . '" -d "password=' . $password . '" https://vimail.cc/admin/mail/users/password -k --user service@vimail.cc:VimailCC2018!');

            if (strpos($returnValue, "OK") !== false) {
                $_SESSION['message'] = "Your new password got set successfully.";
            } else {
                $_SESSION['message'] = "Sorry, there was an error on our side. Please try again later.";
            }
            closeConnection();
            session_destroy();
        } else {
            $_SESSION['message'] = "Sorry, your token is incorrect. Please try again.";
        }
    } else {
        $_SESSION['message'] = "Sorry, this email does not exist. Please try again.";
    }
}
generateBody();